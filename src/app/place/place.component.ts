import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatBottomSheetRef, MatBottomSheet } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.scss']
})
export class PlaceComponent implements OnInit {

  links = [{
    title: 'Küche', temp: 22.4, devices: [
      { name: 'Licht', state: 'off', type: 1 },
      { name: 'Indireckte Beleuchtung', state: 'on', type: 1 },
      { name: 'Steckdose Schrank 1', state: 'on', type: 2 }
    ]
  }, {
    title: 'Wohnzimmer', temp: 21, devices: [
      { name: 'Licht', state: 'off', type: 1 },
      { name: 'TV -Licht', state: 'off', type: 1 },
      { name: 'Steckdose Schrank 1', state: 'on', type: 2 },
      { name: 'Steckdose Schrank 1', state: 'on', type: 2 }
    ]
  }, {
    title: 'Bad', temp: 19, devices: [
      { name: 'Licht', state: 'off', type: 1 }
    ]
  }, {
    title: 'Kinderzimmer', temp: 22.5, devices: [
      { name: 'Licht', state: 'on', type: 1 },
      { name: 'Steckdosen', state: 'on', type: 2 }
    ]
  }, {
    title: 'Garage', temp: 4.2, devices: [
      { name: 'Licht', state: 'off', type: 1 },
      { name: 'Steckdosen', state: 'off', type: 2 }
    ]
  }, {
    title: 'Außen', temp: 3.2, devices: [
      { name: 'Licht Tür', state: 'off', type: 1 },
      { name: 'Licht Weg', state: 'off', type: 1 },
      { name: 'Licht Einfahrt', state: 'off', type: 1 },
      { name: 'Steckdosen Tür', state: 'off', type: 2 }
    ]
  }];

  isLandscape = false;

  constructor(private _bottomSheet: MatBottomSheet, public dialog: MatDialog, public breakpointObserver: BreakpointObserver) {

    breakpointObserver.observe([
      "(max-width: 1000px)"
    ]).subscribe(result => {

      this.isLandscape = result.matches;
    });
  }

  getStyle() {
    return { 'width': this.isLandscape ? '100%' : '50%' };
  }

  ngOnInit() {
  }

  addLink() {
    this.links.push({ title: 'Kitchen', devices: [], temp: 0 });
  }
  addDevice() {
    this.links[0].devices.push({ name: 'Licht', state: 'on', type: 2 });
  }

  stateChange(l: number, d: number) {
    let device = this.links[l].devices[d];
    device.state = (device.state === 'on' ? 'off' : 'on');
  }

  editDialog(l: number, d: number): void {

    let device = this.links[l].devices[d];

    const dialogRef = this.dialog.open(RenameDialog, {
      width: 'auto',
      data: { name: device.name, type: device.type }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result != undefined) {
        device.name = result;
      }

    });
  }

  openBottomSheet(): void {
    const bottomSheetRef = this._bottomSheet.open(BottomSheetOverview, {
      panelClass: 'bottomSheetRef'
    });

    bottomSheetRef.afterDismissed().subscribe((data: string) => {
      console.log(data);
      switch (data) {
        case 'device':

          break;
        case 'place':

          break;
      }

    });

  }

  local(state: string) {
    return (state === 'on') ? 'An' : 'Aus';
  }

  checked(state: string) {
    return (state === 'on');
  }
}

@Component({
  selector: 'bottom-sheet-overview',
  templateUrl: 'bottom-sheet-overview.html',
})
export class BottomSheetOverview {
  constructor(private _bottomSheetRef: MatBottomSheetRef<BottomSheetOverview>) { }
  onclick(event: string): void {
    this._bottomSheetRef.dismiss(event);
  }
}

@Component({
  selector: 'edit-dialog',
  templateUrl: 'edit-dialog.html',
})
export class RenameDialog {

  constructor(
    public dialogRef: MatDialogRef<RenameDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
