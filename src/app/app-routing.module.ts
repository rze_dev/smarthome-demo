import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaceComponent } from './place/place.component';
import { DeviceComponent } from './device/device.component';

const routes: Routes = [
  { path: '', component: PlaceComponent },
  { path: 'device', component: DeviceComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
